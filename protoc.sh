#!/bin/bash

SERVICE_NAME=$1

protoc --go_out=./golang --go_opt=paths=source_relative \
  --go-grpc_out=./golang --go-grpc_opt=paths=source_relative \
 ./${SERVICE_NAME}/*.proto

 # TODO: exit if protoc command fails

cd ./golang/${SERVICE_NAME}/
go mod init gitlab.com/learn_microservices/proto/golang/${SERVICE_NAME}
go mod tidy
cd ../../
